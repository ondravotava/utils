<?php
/**
 * User: ondrej.votava
 * Date: 29. 7. 2014
 * Time: 15:55
 */

/**
 * Class FormRendering
 * @package App\Components\GUI
 * @author ondrej.votava
 * @description : Nastavení vykreslování pro Twitter Boostrap 3.0
 */


namespace CreativeDesign\Utils\UI\Rendering;

use Nette\Application\UI\Form;
use Nette\Forms\Controls;

class FormToBootstrap
{

    /**
     * @param Form $form
     * @param bool $usedPrimary volba css pro button
     * @param bool $horizontal prida se ccc form-horizontal
     *
     * @return Form
     */
    public static function setRender(Form $form, $usedPrimary = TRUE, $horizontal = true)
    {
        // setup form rendering
        $renderer = $form->getRenderer();
        $renderer->wrappers['controls']['container'] = NULL;
        $renderer->wrappers['pair']['container'] = 'div class=form-group';
        $renderer->wrappers['pair']['.error'] = 'has-error';
        $renderer->wrappers['control']['container'] = 'div class=col-md-10';
        $renderer->wrappers['label']['container'] = 'div class="col-md-2 control-label"';
        $renderer->wrappers['control']['description'] = 'span class=help-block';
        $renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';

        // make form and controls compatible with Twitter Bootstrap
        if ($horizontal) {
            $form->getElementPrototype()->class('form-horizontal');
        }


        foreach ($form->getControls() as $control) {
            if ($control instanceof Controls\Button) {
                $control->getControlPrototype()->addClass($usedPrimary ? 'btn btn-primary' : 'btn btn-default');


            } elseif ($control instanceof Controls\TextBase || $control instanceof Controls\SelectBox || $control instanceof Controls\MultiSelectBox) {
                $control->getControlPrototype()->addClass('form-control');

            } elseif ($control instanceof Controls\Checkbox || $control instanceof Controls\CheckboxList || $control instanceof Controls\RadioList) {
                $control->getSeparatorPrototype()->setName('div')->addClass($control->getControlPrototype()->type);
            }
        }

        return $form;
    }
}