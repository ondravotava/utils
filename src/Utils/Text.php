<?php
/**
 * Created by PhpStorm.
 * User: ondrej.votava
 * Date: 1. 4. 2015
 * Time: 15:12
 */

namespace CreativeDesign\Utils;

use Nette\Utils\Strings;

/**
 * Class Text
 * @package CreativeDesign\Utils
 * @author Ondra Votava <ja@ondravotava.cz>
 *         String Helpers
 */

class Text
{

    /**
     * Is field in double size array
     * @param      $needle
     * @param      $needle_field
     * @param      $haystack
     * @param bool $strict
     *
     * @return bool
     */
    public static function in_array_field($needle, $needle_field, $haystack, $strict = false) {
        if ($strict) {
            foreach ($haystack as $item)
                if (isset($item[$needle_field]) && $item[$needle_field] === $needle)
                    return true;
        }
        else {
            foreach ($haystack as $item)
                if (isset($item[$needle_field]) && $item[$needle_field] == $needle)
                    return true;
        }
        return false;
    }

    /*****************************************************************************************************************
     * Convert $variable to boolean
     * @param      $in
     * @param bool $strict
     *
     * @return bool|null
     */
    public static function toBool($in, $strict=true) {
        $out = false;
        // if not strict, we only have to check if something is false
        if (in_array($in,array('false', 'False', 'FALSE', 'no', 'No', 'n', 'N', '0', 'off',
                               'Off', 'OFF', false, 0, null), true)) {
            $out = false;
        } else if ($strict) {
            // if strict, check the equivalent true values
            if (in_array($in,array('true', 'True', 'TRUE', 'yes', 'Yes', 'y', 'Y', '1',
                                   'on', 'On', 'ON', true, 1), true)) {
                $out = true;
            }
        } else {
            // not strict? let the regular php bool check figure it out (will
            //     largely default to true)
            $out = ($in?true:false);
        }
        return $out;
    }

    /******************************************************************************************************************
     * Check if the color is of valid range
     *
     * @param string $color
     *
     * @return bool
     */
    public static function validateHexColor($color)
    {
        // pokud neni na zacatku hash tag pridame ho
        if (preg_match('[^A-Fa-f0-9]', $color)) {
            $color = '#' . $color;
        }

        // pokud odpovida RegExp vratime true
        if (preg_match('/^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/i', $color)) {
            return TRUE;
        }
        else {

            return FALSE;
        }

    }


    /******************************************************************************************************************
     * Truncate HTML text and restore tags
     * @param string $string
     * @param int    $limit
     * @param string $pad
     * @param string $break
     *
     * @return string
     */
    public static function truncateHtmlText($string, $limit, $pad="  ...", $break=" ") {


        // pokud je text kratší než je požadováno vrátíme celý $string
        if(mb_strlen($string, 'UTF-8') <= $limit) return $string;

        // existuje $break mezi $limit a koncem $string?
        if(false !== ($breakpoint = mb_strpos($string, $break, $limit, "UTF-8")))
        {
            if($breakpoint < mb_strlen($string, 'UTF-8') - 1)
            {
                $string = mb_substr($string, 0, $breakpoint, "UTF-8") . $pad;
            }
        }
        return self::restoreHtmlTags($string, $pad);

        // return $string;
    }

    /**
     * @param string $string
     *
     * @param string $pad
     *
     * @return string
     */
    public static function restoreHtmlTags($string, $pad = "  ...")
    {
        //zkotrolujeme ze jsou vsechny tagy ukoncene (cele) pokud ne tak je odstranime
        $prereg = "#((<[a-z1-9]+(?:\n| ).*)((?:>|$))|(<[a-z](?:>|$)))#miU";
        preg_match_all($prereg, $string, $match);
        $uncomplete = $match[0];
        $uncomplete = array_reverse($uncomplete);

        if(!self::endsWith($uncomplete[0],">"))
        {
            $re = "#(".$uncomplete[0].")$#miU";
            $string = preg_replace($re, $pad, $string);
        }

        // najdeme všechny otevřené tagy
        $re = "#<(?!meta|img|br|hr|input\b)\b([a-z1-9]+)((\n| ).*)?(?<![\/|\/ ])>#imU";
        preg_match_all($re, $string, $match);
        $openedtags = $match[1];

        // najdeme všechny uzavřené tagy
        preg_match_all("#<\/([a-z1-9]+)>#iU", $string, $match);
        $closedtags = $match[1];

        $len_opened = count($openedtags);

        // pokud jsou všechny tagy uzavřeny vrátime $string
        if (count($closedtags) == $len_opened) {

            return $string;

        }

        $openedtags = array_reverse($openedtags);

        // zavřeme tagy
        for ($i = 0; $i < $len_opened; $i++) {

            if (!in_array($openedtags[$i], $closedtags)) {
                $string .= "</" . $openedtags[$i] . ">";
            }

            else {
                unset ($closedtags[array_search($openedtags[$i], $closedtags)]);
            }
        }

        return $string;
    }

    /**
     * @param $haystack
     * @param $needle
     *
     * @return bool
     */
    public static function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }
}
