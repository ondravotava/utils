<?php
/**
 * Created by PhpStorm.
 * User: ondrej.votava
 * Date: 25. 3. 2015
 * Time: 13:45
 */

namespace CreativeDesign\Utils\Texy;

use Nette;
/**
 * Class TexyConfig
 * @package CreativeDesign\Utils\Texy
 * @author Ondra Votava <ja@ondravotava.cz>
 */

class TexyConfig extends Nette\Object
{

    /** @var string  */
    public $imageRoot;
    /** @var string  */
    public $fileRoot;

    /** @var string */
    public $urlHost;


    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->imageRoot = $config['imageRoot'];
        $this->fileRoot = $config['fileRoot'];
        $this->urlHost = $config['urlHost'];
    }
}