<?php
/**
 * Created by PhpStorm.
 * User: ondrej.votava
 * Date: 25. 3. 2015
 * Time: 10:04
 */

namespace CreativeDesign\Utils\Texy;

/**
 * Class FshlHtmlOutput
 * @package CreativeDesign\Utils\Texy
 * @author Ondra Votava <ja@ondravotava.cz>
 */

class FshlHtmlOutput implements \FSHL\Output
{
    private $lastClass = null;

    public function template($part, $class)
    {
        $output = '';
        if ($this->lastClass !== $class) {
            if (null !== $this->lastClass) $output .= '</span>';
            if (null !== $class) $output .= '<span class="' . $class . '">';
            $this->lastClass = $class;
        }
        $part = htmlspecialchars($part, ENT_COMPAT, 'UTF-8');
        if ($this->lastClass && strpos($part, "\n") !== FALSE) {
            $endline = "</span>\n" . '<span class="' . $this->lastClass . '">';
            $part = str_replace("\n", $endline, $part);
        }
        return $output . $part;
    }
    public function keyword($part, $class)
    {
        $output = '';
        if ($this->lastClass !== $class) {
            if (null !== $this->lastClass) $output .= '</span>';
            if (null !== $class) $output .= '<span class="' . $class . '">';
            $this->lastClass = $class;
        }
        return $output . htmlspecialchars($part, ENT_COMPAT, 'UTF-8');
    }
}