<?php
/**
 * Created by PhpStorm.
 * User: ondrej.votava
 * Date: 24. 3. 2015
 * Time: 17:23
 */

namespace CreativeDesign\Utils\Texy;

use FSHL\Highlighter;
use Nette\Http\Url;
use Nette\InvalidArgumentException;
use Nette\Object;
use Nette\Utils\Html;
use Nette\Utils\Strings;
use TexyHeadingModule;
use TexyHtml;


/**
 * Class TexyProcessor
 * @package CreativeDesign\Utils\Texy
 * @author Ondra Votava <ja@ondravotava.cz>
 */

class TexyProcessor extends Object
{

    Const LINE_NUMS_ENABLE = TRUE;
    Const LINE_NUMS_DISABLE = FALSE;
    /**
     * @var  lastTexy
     */
    private $lastTexy;

    /** @var Highlighter */
    private $highlighter;

    private $lineNumsOption;

    private $configurator;

    public function __construct(Highlighter $highlighter,TexyConfig $configurator, $lineNumsOption = self::LINE_NUMS_ENABLE)
    {
        $this->highlighter = $highlighter;
        $this->lineNumsOption = $lineNumsOption;
        $this->configurator = $configurator;
    }

    /**
     * @param $options TexyProcessor::LINE_NUMS_ENABLE | TexyProcessor::LINE_NUMS_DISABLE
     */
    public function setLineNumsOption($options)
    {

        if($options === self::LINE_NUMS_ENABLE || $options === self::LINE_NUMS_DISABLE)
            $this->lineNumsOption = $options;
        else
            throw new InvalidArgumentException('Invalid Argument use TexyProcessor::LINE_NUMS_ENABLE or TexyProcessor::LINE_NUMS_DISABLE');
    }

    /** @var array */
    private static $highlights = array(
        'block/code' => TRUE,
        'block/php' => 'FSHL\Lexer\Php',
        'block/neon' => 'FSHL\Lexer\Neon',
        'block/config' => TRUE, // @todo
        'block/sh' => TRUE, // @todo
        'block/texy' => 'FSHL\Lexer\Texy',
        'block/javascript' => 'FSHL\Lexer\Javascript',
        'block/js' => 'FSHL\Lexer\Javascript',
        'block/css' => 'FSHL\Lexer\Css',
        'block/sql' => 'FSHL\Lexer\Sql',
        'block/html' => 'FSHL\Lexer\Html',
        'block/htmlcb' => 'FSHL\Lexer\Html',
    );

    /**
     * @param $text
     *
     * @return string
     */
    public function process($text)
    {
            return $this->createTexy()->process($text);
    }

    /**
     * @return \Texy
     */
    protected function createTexy()
    {
        $texy = new \Texy();
        // obecné nastavení
        $texy->allowedTags = \Texy::ALL;
        $texy->linkModule->root = '';
        $texy->tabWidth = 4;
        $texy->phraseModule->tags['phrase/strong'] = 'b';
        $texy->phraseModule->tags['phrase/em'] = 'i';
        $texy->phraseModule->tags['phrase/em-alt'] = 'i';
        // nadpisy
        $texy->headingModule->top = 2;
        $texy->headingModule->balancing = TexyHeadingModule::FIXED;
        $texy->headingModule->generateID = TRUE;
        $texy->imageModule->root = $this->configurator->imageRoot;
        $texy->imageModule->fileRoot = $this->configurator->fileRoot;
        // čísla řádků pro twitter bootstrap
        $texy->dtd['pre'][1]['ol'] = 1;
        // vypne generování bílých znaků ve výsledném kódu,
        // aby se neroztahoval kód v elementu <pre>
        $texy->htmlOutputModule->indent = FALSE;
        // <code>
        $texy->addHandler('block', array($this, 'blockHandler'));
        $texy->addHandler('phrase', array($this, 'phraseHandler'));
        // return
        return $this->lastTexy = $texy;
    }

    public function getTexy()
    {

        return $this->createTexy();
    }
    /**
     * Zýrazňuje kód
     * @internal
     */
    public function blockHandler(\TexyHandlerInvocation $invocation, $blockType, $content, $lang, $modifier)
    {
        if (isset(self::$highlights[$blockType])) {
            list(, $lang) = explode('/', $blockType);
        } else {
            return $invocation->proceed($blockType, $content, $lang, $modifier);
        }
        $texy = $invocation->getTexy();
        $content = \Texy::outdent($content);
        // zvýraznění syntaxe
        if (class_exists($lexerClass = self::$highlights[$blockType])) {
            $content = $this->highlighter->highlight($content, new $lexerClass());
        } else {
            $content = htmlspecialchars($content);
        }
        $elPre = \TexyHtml::el('pre');
        if ($modifier) $modifier->decorate($texy, $elPre);

        if($this->lineNumsOption) {
            $elPre->attrs['class'] = 'src-' . strtolower($lang) . ' prettyprint linenums';
            // čísla řádků
            $elOl = $elPre->create('ol', array('class' => 'linenums'));
            foreach (Strings::split($content, '~[\n\r]~') as $i => $line) {
                $elLi = $elOl->create('li', array('class' => 'L' . $i));
                $elLi->create('span', $texy->protect($line, \Texy::CONTENT_BLOCK));
            }
        }
        else {

            $content = $texy->protect($content, \Texy::CONTENT_BLOCK);
            $elPre->attrs['class'] = 'src-' . strtolower($lang) . ' prettyprint';
            $elPre->create('code', $content);
        }

        return $elPre;
    }


    /**
     * @param TexyHandlerInvocation  handler invocation
     * @param string
     * @param string
     * @param TexyModifier
     * @param TexyLink
     * @return TexyHtml|string|FALSE
     */
    public function phraseHandler($invocation, $phrase, $content, $modifier, $link)
    {

        $el = $invocation->proceed();

        if ($el instanceof TexyHtml && $el->getName() === 'a') {
            $url = new Url($el->attrs['href']);
            if ($url->authority != $this->configurator->urlHost) {
                $el->attrs['target'] = '_blank';
            }
        }
        return $el;
    }

}

