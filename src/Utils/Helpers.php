<?php
/**
 * Created by PhpStorm.
 * User: ondrej.votava
 * Date: 25. 3. 2015
 * Time: 8:40
 */

namespace CreativeDesign\Utils;

use Nette;

/**
 * Class Helpers
 * @package App
 * @author Ondra Votava <ja@ondravotava.cz>
 */

class Helpers
{


    /**
     * Pridani helperu v presenteru
     * $this->template->addFilter(NULL, '\CreativeDesign\Utils\Helpers::loader');
     */

    /**
     * @param $filter
     * @param $value
     *
     * @return mixed
     */
     public static function loader($filter, $value)
    {
        if (method_exists(__CLASS__, $filter)) {
            $args = func_get_args();
            \Tracy\Debugger::barDump($args, '$args');
            array_shift($args);
            return call_user_func_array(array(__CLASS__, $filter), $args);
        }
    }


    /**
     * @param $in
     * @param $len
     *
     * @return string
     */
    public static function htmlTruncate($in, $len, $pad="  ...", $break=" ")
    {

        $html = new Nette\Utils\Html();
        return  $html::el()->setHtml(Text::truncateHtmlText($in, $len, $pad, $break));

    }

    /**
     * @param string      $email
     * @param int    $s
     * @param string $d
     * @param string $r
     * @param bool   $img
     * @param array  $atts
     *
     * @return string
     */
    public static function getGavatar($email, $s = 80, $img = false, $atts = array(), $d = 'mm', $r = 'g' )
    {
        $url = 'http://www.gravatar.com/avatar/';
        $url .= md5( strtolower( trim( $email ) ) );
        $url .= "?s=$s&d=$d&r=$r";
        if ( $img ) {
            $url = '<img src="' . $url . '"';
            foreach ( $atts as $key => $val )
                $url .= ' ' . $key . '="' . $val . '"';
            $url .= ' />';
        }
        $html = new Nette\Utils\Html();
        return $html::el()->setHtml($url);

    }

}